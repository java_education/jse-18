package ru.t1.oskinea.tm.command.task;

import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Complete task by id.";

    private static final String NAME = "task-complete-by-id";

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(id, Status.COMPLETED);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
