package ru.t1.oskinea.tm.command.project;

import ru.t1.oskinea.tm.model.Project;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "Remove project by index.";

    private static final String NAME = "project-remove-by-index";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        getProjectTaskService().removeProjectById(project.getId());
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
