package ru.t1.oskinea.tm.command.user;

import ru.t1.oskinea.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "Update profile of current user.";

    private final String NAME = "user-update-profile";

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.print("FIRST NAME: ");
        final String firstName = TerminalUtil.nextLine();
        System.out.print("LAST NAME: ");
        final String lastName = TerminalUtil.nextLine();
        System.out.print("MIDDLE NAME: ");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
