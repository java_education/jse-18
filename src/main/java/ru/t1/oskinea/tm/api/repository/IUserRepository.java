package ru.t1.oskinea.tm.api.repository;

import ru.t1.oskinea.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    List<User> findAll();

    User findByEmail(String email);

    User findById(String id);

    User findByLogin(String login);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User remove(User user);

}
