package ru.t1.oskinea.tm.api.model;

import ru.t1.oskinea.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
